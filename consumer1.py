import pika, sys, os
import time
import pika, sys, os
from typing import List
import json
import requests
import urllib
import urllib.request
import pandas as pd
import time
import threading


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    
    sourceCollection = json.loads(body)
    #Добавить семафор чтобы не ддосить, если пришло много сообщений из очереди одновременно
    all_items_thread = threading.Thread(target=all_items, name="DownloadAll", args=[sourceCollection])
    all_items_thread.start()

    # ch.basic_ack(delivery_tag = method.delivery_tag)

def all_items(sourceCollection):
    headers = get_headers()
    for source in sourceCollection['sources']:
        download_thread = threading.Thread(target=load_items, name="Downloader:" + str(source['id']), args=[source['id'], headers, source['name']])
        download_thread.start()
        time.sleep(10)
        # load_items(source['id'],headers, source['name'])

def load_items(id_store, headers, name):
    print('Starting for ' + str(id_store) + ', name: '+ str(name))
    try:
        start_time = time.time()
        link = get_feed(id_store, headers, name)
        if link == None:
            pass
        else:
            prepare(link)
        elapsedSeconds = int(time.time() - start_time)
        send(id_store,link,elapsedSeconds)
        print('Finished for ' + str(id_store) +', name: '+ str(name) + ', elapsed time:' +str(elapsedSeconds))
    except Exception as err:
        print('Error in {0} store name {1}, error name: {3}'.format(id_store, name, err))

def get_headers():
    headers = {
        'Authorization': 'Basic ZDBjYjNkMWU2OTc2YWQ0MWM2NmNiMmQyOWVhNDQyOmNlYWVjOWM5NTVhMGQxYzY5NTZjNmFhNmFlZmI1Yw==',
    }

    data = {
      'grant_type': 'client_credentials',
      'client_id': 'd0cb3d1e6976ad41c66cb2d29ea442',
      'scope': 'advcampaigns banners websites advcampaigns_for_website'
    }

    response = requests.post('https://api.admitad.com/token/', headers=headers, data=data)

    token = json.loads(response.text)['access_token']

    headers = {
        'Authorization': 'Bearer '+str(token),
    }
    return headers

def get_feed(id_store,headers,name):
    SOURCE = '/home/day_feeds/'
    response = requests.get('https://api.admitad.com/advcampaigns/'+str(id_store)+'/website/1324021/',headers=headers).json()
    if name == 'lamoda ru':
        urllib.request.urlretrieve('http://export.admitad.com/ru/webmaster/websites/1324021/products/export_adv_products/?user=fashionai&code=cs59jmqv8t&feed_id=17684&format=csv',SOURCE+name+'.csv')
        return SOURCE+name+'.csv'
    else:
        try:
            urllib.request.urlretrieve(response['products_csv_link'], SOURCE+name+'.csv')
            return SOURCE+name+'.csv'
        except:
            return None
            print(name, 'None')
def get_color(row):
    for i in row:
        if 'color' in i:
            return(i.split(':')[-1].strip())
        if  'цвет' in i:
            return(i.split(':')[-1].strip())

def get_size(row):
    for i in row:
        if 'size' in i:
            return(i.split(':')[-1].strip())
        if  'размер' in i:
            return(i.split(':')[-1].strip())

def get_gender(row):
    for i in row:
        if 'пол' in i:
            if 'пол' == i.split(':')[0]:
                return(i.split(':')[-1])
        if 'gender' in i:
            return(i.split(':')[-1])

def get_dict():
    list_line = []
    with open("./dict/clt_dict.txt", "r") as file:
        for line in file:
            list_line.append(line.replace('\n',''))
    return list_line

def prepare(link):
    df = pd.read_csv(link, sep = ';', error_bad_lines=True)
    snt_list = []
    list_clt = list(set(get_dict()))
    for line in df['name'].unique():
        for clt in list_clt:
            try:
                if clt.lower() in line.lower():
                    snt_list.append(line)
            except:
                continue
    result_df = df[df['name'].isin(snt_list)].reset_index(drop = True)
    list_columns = ['categoryId', 'id','description','oldprice','price','picture','url','color','size','gender','vendor']
    try:
        result_df['list_param'] = result_df['param'].apply(lambda x: x.lower().split('|'))
        result_df['color'] = result_df['list_param'].apply(get_color)
        result_df['size'] = result_df['list_param'].apply(get_size)
        result_df['gender'] = result_df['list_param'].apply(get_gender)
        result_df[list_columns].to_csv(link,sep = ';')
    except:
        print(link.split('/')[-1][:-3])

class LoadedSource:
    id: int
    filePath: str
    elapsedSeconds: int

    def __init__(self, id: int, filePath: str, elapsedSeconds: int) -> None:
        self.id = id
        self.filePath = filePath
        self.elapsedSeconds = elapsedSeconds


# class Source:
#     id: int
#     name: str

#     def __init__(self, id: int, name: str) -> None:
#         self.id = id
#         self.name = name


# class ScheduledMessage:
#     sources: List[Source]

#     def __init__(self, sources: List[Source]) -> None:
#         self.sources = sources

def send(id_store, link, elapsedSeconds):
    credentials = pika.PlainCredentials('lucfashion', 'luc_rabbit_123')
    parameters = pika.ConnectionParameters(host = '194.67.203.143',port = 5672,credentials = credentials, heartbeat = 0)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='filesProcessing')
    result  = LoadedSource(id_store,link,elapsedSeconds)
    send_json = json.dumps(result.__dict__)
    channel.basic_publish(exchange='', routing_key='filesProcessing', body=send_json)
    connection.close()

def main():
    credentials = pika.PlainCredentials('lucfashion', 'luc_rabbit_123')
    parameters = pika.ConnectionParameters(host = '194.67.203.143',port = 5672,credentials = credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    channel.queue_declare(queue='scheduler')


    channel.basic_consume(queue='scheduler', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()





if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
